import 'package:flutter/material.dart';

Widget _checkBox(String title, bool value, ValueChanged<bool?>? onChanged) {
  return Container(
      padding: EdgeInsets.fromLTRB(16, 4, 16, 4),
      child: Column(children: [
        Row(
          children: [
            Text(title),
            Expanded(
              child: Container(),
            ),
            Checkbox(
                value: value,
                onChanged: onChanged,
            ),
          ],
        ),
        Divider()
      ]));
}

class CheckBoxWidget extends StatefulWidget {
  CheckBoxWidget({Key? key}) : super(key: key);

  @override
  _CheckBoxWidgetState createState() => _CheckBoxWidgetState();
}

class _CheckBoxWidgetState extends State<CheckBoxWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  bool check4 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('CheckBox')),
        body: ListView(
          children: [
            _checkBox('Check1', check1, (value) {
              setState(() {
                check1 = value!;
              });
            }),
            _checkBox('Check2', check2, (value) {
              setState(() {
                check2 = value!;
              });
            }),
            _checkBox('Check3', check3, (value) {
              setState(() {
                check3 = value!;
              });
            }),
            _checkBox('Check4', check4, (value) {
              setState(() {
                check4 = value!;
              });
            }),
            TextButton(
              child: Text('Save'),
              onPressed: () {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text(
                    'check1: $check1, check2: $check2, check3: $check3, check4: $check4'
                  )));
              }
            )
          ],
        ));
  }
}
