import 'package:flutter/material.dart';

class DropDownWidget extends StatefulWidget {
  DropDownWidget({Key? key}) : super(key: key);

  @override
  _DropDownwidgetState createState() => _DropDownwidgetState();
}

class _DropDownwidgetState extends State<DropDownWidget> {
  String vaccine = '-';
  
  @override 
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('DropDown'),),
      body: Column(
        children: [
          Container(
            margin: EdgeInsets.all(16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Vaccine: '),
                DropdownButton(
                  items: [
                    DropdownMenuItem(child: Text('-'), value: '-'),
                    DropdownMenuItem(child: Text('Pfizer'), value: 'Pfizer'),
                    DropdownMenuItem(child: Text('Johnson'), value: 'Johnson'),
                    DropdownMenuItem(child: Text('Sputnik V'), value: 'Sputnik V'),
                    DropdownMenuItem(child: Text('AstraZeneca'), value: 'AstraZeneca'),
                    DropdownMenuItem(child: Text('Sinopharm'), value: 'Sinopharm'),
                    DropdownMenuItem(child: Text('Sinovac'), value: 'Sinovac'),
                  ],
                  value: vaccine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                )
              ],
            )
          ),
          Center(
            child: Text(vaccine, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
          )
        ],
      ),
    );
  }
}